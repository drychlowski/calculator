﻿using System.Windows;

namespace Calculator
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var window = new MainWindow {DataContext = new CalculatorViewModel(new CalculatorLogic())};
            window.Show();
        }

    }
}
