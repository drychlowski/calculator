﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    abstract class CalculatorBaseState : ICalculatorOperations
    {
        protected readonly CalculatorLogic calculatorLogic;

        protected CalculatorBaseState(CalculatorLogic calculatorLogic)
        {
            this.calculatorLogic = calculatorLogic;
            Result = this.calculatorLogic.Result;
            History = this.calculatorLogic.History;
        }

        protected string AccumulatedResult
        {
            get => calculatorLogic.AccumulatedResult;
            set => calculatorLogic.AccumulatedResult = value;
        }

        protected string Result
        {
            get => calculatorLogic.Result;
            set => calculatorLogic.Result = value;
        }

        protected string History
        {
            get => calculatorLogic.History;
            set => calculatorLogic.History = value;
        }

        public abstract void InsertDigit(string digit);

        public abstract void InsertOperation(string operation);

        public abstract void Backspace();

        public abstract void Equal();

        public void Clear()
        {
            History = string.Empty;
            Result = "0";
            AccumulatedResult = "0";
        }

        protected long Calculate(long a, long b, string operation)
        {
            switch (operation)
            {
                case "+":
                    return a + b;

                case "-":
                    return a - b;

                case "*":
                    return a * b;

                case "/":
                    return a / b;

                default:
                    return 0;
            }
        }
    }
}
