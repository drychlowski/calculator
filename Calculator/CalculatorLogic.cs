﻿namespace Calculator
{
    class CalculatorLogic : ICalculatorOperations
    {
        public string AccumulatedResult { get; set; }
        public string Result { get; set; }
        public string History { get; set; }
        public ICalculatorOperations CalculatorState { get; set; }

        public CalculatorLogic()
        {
            Result = "0";
            History = string.Empty;
            CalculatorState = new DigitInsertedState(this);
        }

        public void InsertDigit(string digit)
        {
            CalculatorState.InsertDigit(digit);
        }

        public void InsertOperation(string operation)
        {
            CalculatorState.InsertOperation(operation);
        }

        public void Backspace()
        {
            CalculatorState.Backspace();
        }

        public void Clear()
        {
            CalculatorState.Clear();
        }

        public void Equal()
        {
            CalculatorState.Equal();
        }
    }
}
