﻿using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Calculator
{
    class CalculatorViewModel : BaseViewModel
    {
        private readonly CalculatorLogic calculatorLogic;

        public string Result => long.TryParse(calculatorLogic.Result, out var temp) ? temp.ToString("#,##0") : calculatorLogic.Result;
        public string History => Regex.Replace(calculatorLogic.History, "[-,+,*,/]", " $0 ");

        public ICommand InsertDigitCommand { get; }
        public ICommand BackspaceCommand { get; }
        public ICommand ClearCommand { get; }
        public ICommand InsertOperationCommand { get; }
        public ICommand EqualCommand { get; }


        public CalculatorViewModel(CalculatorLogic calculatorLogic)
        {
            this.calculatorLogic = calculatorLogic;
            InsertDigitCommand = new Command(InsertDigit);
            BackspaceCommand = new Command(Backspace);
            ClearCommand = new Command(Clear);
            InsertOperationCommand = new Command(InsertOperation);
            EqualCommand = new Command(Equal);
        }

        private void InsertDigit(object digit)
        {
            calculatorLogic.InsertDigit((string)digit);
            OnPropertyChanged(nameof(Result));
        }

        private void InsertOperation(object operation)
        {
            calculatorLogic.InsertOperation((string)operation);
            OnPropertyChanged(nameof(Result));
            OnPropertyChanged(nameof(History));
        }

        private void Backspace()
        {
            calculatorLogic.Backspace();
            OnPropertyChanged(nameof(Result));
        }

        private void Clear()
        {
            calculatorLogic.Clear();
            OnPropertyChanged(nameof(Result));
            OnPropertyChanged(nameof(History));
        }

        private void Equal()
        {
            calculatorLogic.Equal();
            OnPropertyChanged(nameof(Result));
            OnPropertyChanged(nameof(History));
        }
    }
}
