﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculator
{
    class Command : ICommand
    {
        private readonly Action<object> _execute;
        public event EventHandler CanExecuteChanged;

        public Command(Action<object> execute)
        {
            _execute = execute;
        }

        public Command(Action action) : this(o => action())
        { }

        void ICommand.Execute(object parameter)
        {
            _execute(parameter);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return true;
        }
    }
}
