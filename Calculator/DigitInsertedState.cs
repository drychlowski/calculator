﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class DigitInsertedState : CalculatorBaseState
    {
        public DigitInsertedState(CalculatorLogic calculatorLogic) : base(calculatorLogic)
        {
        }

        public override void InsertDigit(string digit)
        {
            if (Result.Length > 15) return;
            Result = Result.Equals("0") ? digit : Result += digit;
        }

        public override void InsertOperation(string operation)
        {
            try
            {
                AccumulatedResult = History.Length > 0
                    ? Convert.ToString(Calculate(long.Parse(AccumulatedResult), long.Parse(Result), History.Substring(History.Length - 1, 1)))
                    : Result;
            }
            catch (DivideByZeroException)
            {
                Clear();
                Result = "Can not divide by zero.";
                calculatorLogic.CalculatorState = new DividedByZeroState(calculatorLogic);
                return;
            }

            History += Result + operation;

            Result = AccumulatedResult;

            calculatorLogic.CalculatorState = new OperationInsertedState(calculatorLogic);
        }

        public override void Backspace()
        {
            if (string.IsNullOrEmpty(Result)) return;

            Result = Result.Length <= 1 ? "0" : Result.Remove(Result.Length - 1, 1);
        }

        public override void Equal()
        {
            calculatorLogic.CalculatorState = new EqualState(calculatorLogic);
            calculatorLogic.CalculatorState.Equal();
        }
    }
}
