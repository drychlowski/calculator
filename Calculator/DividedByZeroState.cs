﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class DividedByZeroState : CalculatorBaseState
    {
        public DividedByZeroState(CalculatorLogic calculatorLogic) : base(calculatorLogic)
        {
            
        }

        public override void InsertDigit(string digit)
        {
            Result = String.Empty;
            calculatorLogic.CalculatorState = new DigitInsertedState(calculatorLogic);
            calculatorLogic.CalculatorState.InsertDigit(digit);
        }

        public override void InsertOperation(string operation)
        {
            return;
        }

        public override void Backspace()
        {
            return;
        }

        public override void Equal()
        {
            return;
        }
    }
}
