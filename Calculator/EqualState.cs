﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class EqualState : CalculatorBaseState
    {
        private string lastOperation;
        private string lastNumber;

        public EqualState(CalculatorLogic calculatorLogic) : base(calculatorLogic)
        {
        }

        public override void InsertDigit(string digit)
        {
            Result = string.Empty;
            calculatorLogic.CalculatorState = new DigitInsertedState(calculatorLogic);
            calculatorLogic.CalculatorState.InsertDigit(digit);
        }

        public override void InsertOperation(string operation)
        {
            calculatorLogic.CalculatorState = new DigitInsertedState(calculatorLogic);
            calculatorLogic.CalculatorState.InsertOperation(operation);
        }

        public override void Backspace()
        {
            return;
        }

        public override void Equal()
        {
            if (History.Length > 0)
            {
                lastOperation = History.Substring(History.Length - 1, 1);
                lastNumber = Result;
            }
                
            try
            {
                AccumulatedResult = lastOperation != null
                    ? Convert.ToString(Calculate(long.Parse(AccumulatedResult), long.Parse(lastNumber), lastOperation))
                    : Result;
            }
            catch (DivideByZeroException)
            {
                Clear();
                Result = "Can not divide by zero.";
                calculatorLogic.CalculatorState = new DividedByZeroState(calculatorLogic);
                return;
            }

            Result = AccumulatedResult;
            History = string.Empty;
        }
    }
}
