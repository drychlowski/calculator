﻿namespace Calculator
{
    interface ICalculatorOperations
    {
        void InsertDigit(string digit);
        void InsertOperation(string operation);
        void Backspace();
        void Clear();
        void Equal();
    }
}
