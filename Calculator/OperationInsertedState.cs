﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class OperationInsertedState : CalculatorBaseState
    {
        public OperationInsertedState(CalculatorLogic calculatorLogic) : base(calculatorLogic)
        {
        }

        public override void InsertDigit(string digit)
        {
            Result = string.Empty;

            calculatorLogic.CalculatorState = new DigitInsertedState(calculatorLogic);
            calculatorLogic.CalculatorState.InsertDigit(digit);
        }

        public override void InsertOperation(string operation)
        {
            if (History.Length <= 0) return;

            History = History.Remove(History.Length - 1, 1) + operation;
        }

        public override void Backspace()
        {
            return;
        }

        public override void Equal()
        {
            calculatorLogic.CalculatorState = new EqualState(calculatorLogic);
            calculatorLogic.CalculatorState.Equal();
        }
    }
}
